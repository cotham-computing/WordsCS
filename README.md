# WordsCS
AQA Pre-release 2018

Bug Tracker (sheet): [HERE](https://docs.google.com/document/d/1Dett7EJMl_bPcX0cf28oM_WkApZdYZreBF9iOO2Js7A/edit)  
Bug Tracker (GitLab): [HERE](https://gitlab.com/cotham-computing/WordsCS/issues)

## Repository Access
Read access is public.  
Ask Jack or Jed for write access.

## Contributing
Please use your own branch for development. *DO NOT PUSH STRAIGHT TO MASTER THANKS.*